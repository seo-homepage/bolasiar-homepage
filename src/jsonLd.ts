import siteData from './siteConfig'

const Organization = {
  '@context': 'https://schema.org',
  '@type': 'Organization',
  'name': siteData.siteName,
  'alternateName': siteData.siteNameAlt,
  'url': siteData.siteUrl,
  'logo': siteData.logo,
  'sameAs': siteData.siteUrl,
}

const Article = {
  '@context': 'https://schema.org',
  '@type': 'Article',
  'mainEntityOfPage': {
    '@type': 'WebPage',
    '@id': siteData.siteUrl,
  },
  'headline': siteData.title,
  'description': siteData.description,
  'image': `['${siteData.featuredImage}']`,
  'author': {
    '@type': 'Organization',
    'name': siteData.siteName,
    'url': siteData.siteUrl,
  },
  'publisher': {
    '@type': 'Organization',
    'name': siteData.siteName,
    'logo': {
      '@type': 'ImageObject',
      'url': siteData.logo,
    },
  },
}
const Breadcrumbs = {
  '@context': 'https://schema.org/',
  '@type': 'BreadcrumbList',
  'itemListElement': [{
    '@type': 'ListItem',
    'position': 1,
    'name': 'home',
    'item': siteData.siteUrl,
  },
  {
    '@type': 'ListItem',
    'position': 2,
    'name': 'link-terbaru',
    'item': siteData.siteUrl,
  },
  {
    '@type': 'ListItem',
    'position': 3,
    'name': siteData.title,
  }],
}
export {
  Organization,
  Article,
  Breadcrumbs,
}
