import ampCustom from "./styles/amp-custom.css?inline";
import ampRuntome from "./styles/amp-runtime.css?inline";
import siteData from "../src/siteConfig";
import { Organization, Article, Breadcrumbs } from "./jsonLd";

useHead({
title: siteData.title,
meta: [
{ name: "description", content: siteData.description },
{
name: "theme-color",
content: () => (isDark.value ? "#00aba9" : "#ffffff"),
},
],
link: [
{
rel: "icon",
type: "image/svg+xml",
href: () => (preferredDark.value ? "/favicon-dark.svg" : "/favicon.svg"),
},
],
style: [
{
"amp-runtime": "",
"i-amphtml-version": "012107240354000",
innerHTML: ampRuntome,
},
{
"amp-custom": "",
"i-amphtml-version": "012107240354000",
innerHTML: ampCustom,
},
],
script: [
{
async: "true",
type: "module",
src: "https://cdn.ampproject.org/v0.mjs",
crossorigin: "anonymous",
},

{
async: "true",
type: "module",
"custom-element": "amp-carousel",
src: "https://cdn.ampproject.org/v0/amp-carousel-0.1.mjs",
crossorigin: "anonymous",
},
{
async: "true",
"custom-element": "amp-install-serviceworker",
type: "module",
src: "https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.mjs",
crossorigin: "anonymous",
},
{
"custom-element": "amp-youtube",
async: "true",
type: "module",
src: "https://cdn.ampproject.org/v0/amp-youtube-0.1.mjs",
crossorigin: "anonymous",
},
{
async: "true",
"custom-element": "amp-accordion",
type: "module",
src: "https://cdn.ampproject.org/v0/amp-accordion-0.1.mjs",
crossorigin: "anonymous",
},

{
type: "application/ld+json",
children: JSON.stringify(Organization),
},
{
type: "application/ld+json",
children: JSON.stringify(Article),
},
{
type: "application/ld+json",
children: JSON.stringify(Breadcrumbs),
},
],
});
